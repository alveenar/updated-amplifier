/////////////////////////////////////////////////////////////////////////////////
///
/// Copyright(c) COSEDA Technologies GmbH.
///              All rights reserved.
///              (customizable in COPYRIGHT file)
///
/////////////////////////////////////////////////////////////////////////////////
//
// @project      updated_amplifier
//
// @library      amp_lib
//
// @file         power_calc
//
// @brief
//     
//
// DESCRIPTION OF PORTS:           (please see header)
//
// this file was generated by 'sca_xml2cpp.xsl' StyleSheet
//
////////////////////////////////////////////////////////////////////////////////
//
// Version Id       : $Id$
// @author          : Alveena Rifat
// Last Modified By : $Author$
// Last Modified On : $Date$
//
////////////////////////////////////////////////////////////////////////////////

#ifndef UPDATED_AMPLIFIER_AMP_LIB_POWER_CALC_H_
#include "power_calc.h"
#define COSIDE_INCLUDE_IMPLEMENTATION
#endif

// adds SystemC namespaces for user convenience
#include <systemc-ams.h>

namespace updated_amplifier_namespace
{

//-------------------------------------------------------------------//
// states                                                            //
//-------------------------------------------------------------------//
struct power_calc::states
{

    states(const params& p)
    {
    }

};
//-------------------------------------------------------------------//

#ifdef COSIDE_INCLUDE_IMPLEMENTATION


//-------------------------------------------------------------------//
power_calc::states& power_calc::create_states(const params& pa) { return *(new states(pa)); }
//-------------------------------------------------------------------//


//////////////////////////////////////////////
// method called by constructor             //
//////////////////////////////////////////////
void power_calc::construct()
{
	samples = 0;
	previous_mean = 0;
}

//////////////////////////////////////////////
// destructor                               //
//////////////////////////////////////////////
power_calc::~power_calc()
{

    delete &s;
}

//////////////////////////////////////////////
// method initialize                        //
//////////////////////////////////////////////
void power_calc::initialize()
{

}

//////////////////////////////////////////////
// method processing                        //
//////////////////////////////////////////////
void power_calc::processing()
{
	    samples++;
		double signal_val = input_signal.read();
		double temp_val = pow (abs(signal_val),2);
		prev_value += temp_val; // for testbench, where this values is divided

		previous_mean = (previous_mean * (samples - 1) + temp_val)/samples; // to show on graph

		power_out.write(prev_value);
}

//////////////////////////////////////////////
// method called by set_attributes          //
//////////////////////////////////////////////
void power_calc::set_attributes_cpp()
{

}



#endif // #ifdef COSIDE_INCLUDE_IMPLEMENTATION

}  // end namespace updated_amplifier_namespace

// clear temporary defines
#undef COSIDE_INCLUDE_IMPLEMENTATION
