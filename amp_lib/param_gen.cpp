/////////////////////////////////////////////////////////////////////////////////
///
/// Copyright(c) COSEDA Technologies GmbH.
///              All rights reserved.
///              (customizable in COPYRIGHT file)
///
/////////////////////////////////////////////////////////////////////////////////
//
// @project      updated_amplifier
//
// @library      amp_lib
//
// @file         param_gen
//
// @brief
//     
//
// DESCRIPTION OF PORTS:           (please see header)
//
// this file was generated by 'sca_xml2cpp.xsl' StyleSheet
//
////////////////////////////////////////////////////////////////////////////////
//
// Version Id       : $Id$
// @author          : Alveena Rifat
// Last Modified By : $Author$
// Last Modified On : $Date$
//
////////////////////////////////////////////////////////////////////////////////

#ifndef UPDATED_AMPLIFIER_AMP_LIB_PARAM_GEN_H_
#include "param_gen.h"
#define COSIDE_INCLUDE_IMPLEMENTATION
#endif

// adds SystemC namespaces for user convenience
#include <systemc-ams.h>
using namespace std;
namespace updated_amplifier_namespace
{

//-------------------------------------------------------------------//
// states                                                            //
//-------------------------------------------------------------------//
struct param_gen::states
{

    states(const params& p)
    {
    }

};
//-------------------------------------------------------------------//

#ifdef COSIDE_INCLUDE_IMPLEMENTATION


//-------------------------------------------------------------------//
param_gen::states& param_gen::create_states(const params& pa) { return *(new states(pa)); }
//-------------------------------------------------------------------//


//////////////////////////////////////////////
// method called by constructor             //
//////////////////////////////////////////////
void param_gen::construct()
{
	counter=0;
}

//////////////////////////////////////////////
// destructor                               //
//////////////////////////////////////////////
param_gen::~param_gen()
{

    delete &s;
}

//////////////////////////////////////////////
// method initialize                        //
//////////////////////////////////////////////
void param_gen::initialize()
{
	string line,str;
			 double a,b,c;

						 ifstream myfile;
						 myfile.open("D:\\workspace\\param_file.txt");
						 if (myfile.is_open())
						  {
							 while (getline(myfile,line))
							  {
							     istringstream iss(line);
							     while(iss>>str>>a>>b>>c)
							      {
							    	  param_name.push_back(str);
							    	  first.push_back(a);
							    	  last.push_back(b);
							    	  step_size.push_back(c);

							      }

							   }

							  myfile.close();


						  }

						else cout << "Unable to open file";

	    for(int i=0;i<param_name.size();i++) {
	    param_combi param1 (param_name.at(i),first.at(i), last.at(i), step_size.at(i));
		parameters.push_back(param1);
		}
		rand_function sine_func ("sin(2*pi*f*t)", parameters, 1, 0.5);
	 	gen.generate_signals(sine_func);
	 	gnrtd_signals = gen.get_signals();


	 	//open file for writing
	 	ofstream fw("D:\\workspace\\Param_Combi.txt", std::ofstream::out);
	 	//check if file was successfully opened for writing
	 	if (fw.is_open())
	 	{
	 	  //store vector of amp and freq contents to text file
	 		for(int i=0;i<gnrtd_signals.size();i++)
	 		 	{
	 		 		//amp.push_back(gnrtd_signals.at(i).get_signal_amplitude());
	 		 		//freq.push_back(gnrtd_signals.at(i).get_signal_frequency());
	 		 		 fw << gnrtd_signals.at(i).get_signal_amplitude()<<" ";
	 		 		 fw << gnrtd_signals.at(i).get_signal_frequency() << "\n";
	 		 	}

	 	  fw.close();
	 	}
	 	else cout << "Problem with opening file";

	 	param_index = 0;
	 	counter = 0;
	 	sample_count=9; // the no. of samples created for each combination
	 	std::cout<<" combinations done"<<endl;
	 	//threshold=10e-6;
	 	//sig_done.write(1);

}

//////////////////////////////////////////////
// method processing                        //
//////////////////////////////////////////////
void param_gen::processing()
{
  //param_index=0;
		    //std::cout<<gnrtd_signals.size()<<"  "<<"size of gnrtd_signals"<<endl;
	//cout<<param_index<<" param index outside"<<endl;
  if (start_processing.read()==1)

  {

            if (param_index < gnrtd_signals.size()) //for(int i=0; i<(gnrtd_signals.size()* sample_count)+(gnrtd_signals.size()-1);i++)

			{   //cout<<param_index<<" param index"<<endl;
            	        all_values_processed.write(0);
						   if (counter==9)   // to get 9 samples of each combination     //if (time_difference>9e-6 && time_difference<11e-6)

								{

									//if(param_index!=(gnrtd_signals.size()-1))
										//update new values
									//{

										counter=0;
										//cout<<counter<<"  samples reset"<<endl;
									   //std::cout<<current_time<<"  "<<"current time"<<endl;
										//double amp = gnrtd_signals.at(param_index+1).get_signal_amplitude();
										//double freq = gnrtd_signals.at(param_index+1).get_signal_frequency();
										//cout << "amp_new - " << amp <<"  "<<"freq_new - " << freq << endl;
										amp_out.write(0);
										freq_out.write(0);
										//counter++;
										samples.write(counter);
										param_index++;
										//last_timestep=current_time;
										new_values_updated.write(1);

									//}


								}

							else
								{
									// write old values
									new_values_updated.write(0);
									double amp = gnrtd_signals.at(param_index).get_signal_amplitude();
									double freq = gnrtd_signals.at(param_index).get_signal_frequency();

									//cout << "amp_old - " << amp <<"  "<< " freq_old - " << freq << endl;
									//current_time = current_time + 1e-6;      //since the current time doesnot update inside while loop
									//std::cout<<current_time<<"  "<<"current time"<<endl;
									counter ++ ;
									//cout<<counter<<"  counter"<<endl;
									amp_out.write(amp);
									freq_out.write(freq);
									samples.write(counter);


								 }


				}

            else
            {
            	all_values_processed.write(1);
            	param_index=0;
            }

  }



}

//////////////////////////////////////////////
// method called by set_attributes          //
//////////////////////////////////////////////
void param_gen::set_attributes_cpp()
{
	amp_out.set_timestep(sca_core::sca_time(1,sc_core::SC_US));
	freq_out.set_timestep(sca_core::sca_time(1,sc_core::SC_US));
	new_values_updated.set_timestep(sca_core::sca_time(1,sc_core::SC_US));
	all_values_processed.set_timestep(sca_core::sca_time(1,sc_core::SC_US));
}



#endif // #ifdef COSIDE_INCLUDE_IMPLEMENTATION

}  // end namespace updated_amplifier_namespace

// clear temporary defines
#undef COSIDE_INCLUDE_IMPLEMENTATION
