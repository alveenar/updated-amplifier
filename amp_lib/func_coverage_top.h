// #include "../fc4sc/includes/fc4sc.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include "D:\workspace\fc4sc-master\includes\fc4sc.hpp"
#include <iostream>
#include <stdint.h>
#include <bits/stdc++.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <tuple>
#include<algorithm>
using namespace std;

double amp_lower_limit = 0;
double amp_upper_limit = 0;
double amp_step_size = 0;
double freq_lower_limit = 0;
double freq_upper_limit = 0;
double freq_step_size = 0;
double gain_lower_limit = 0;
double gain_upper_limit = 0;
double gain_step_size = 0;
string line, str;

// std::tuple<double, double,double>




std::vector<double> make_bins_amps (double amp_ll, double amp_ul, double amp_ss)

{
   // double ampValues = amp_ul / amp_ss;

//	upper_limit = amp_ul;
//	lower_limit = amp_ll;
//	step_size = amp_ss;
//	cout<< "func make_bins_amps called" <<endl;

	std::vector<double> amps;

	for (double i = amp_ll ; i <= amp_ul ; i += amp_ss)
	{
		amps.push_back(i);
		//cout << amps[i] << ' ';

	}

	return amps;

}
std::vector<double> make_bins_freq (double freq_ll, double freq_ul, double freq_ss)

{
	std::vector<double> freq;

	for (double i = freq_ll ; i <= freq_ul ; i += freq_ss)
	{
		freq.push_back(i);
		//cout << freq[i] << ' ';

	}

	return freq;

}
std::vector<double> make_bins_power (double pow_ll, double pow_ul, double pow_ss)

{
    double boundary_limit = 0.0001 * pow_ll ;
	std::vector<double> pow;

	for (double i = pow_ll ; i <= pow_ul ; i+= pow_ss)
	{
		//std::cout<<i << "i "<<endl;
		pow.push_back(i - boundary_limit);
		//std::cout<<i - five_percent<< "i- "<<endl;
		pow.push_back(i + boundary_limit);
		//std::cout<<i + five_percent<< "i+ "<<endl;
     i=i+1;
	}

	return pow;

}

std::vector<std::pair<double, double>> make_intervals(double ll, double ul, double ss)
		{

	std::vector<double> power_range = make_bins_power ( ll, ul , ss);
	 std::vector<std::pair<double, double>> vec_of_pairs;
	 //std::cout<<"size of power_range  "<<power_range.size()<<endl;
	 for(int i=0;i<power_range.size()-1;i++){
	 vec_of_pairs.push_back(std::make_pair(power_range.at(i),power_range.at(i+1)));
	 std::cout << "(" << vec_of_pairs[i].first << "," << vec_of_pairs[i].second << ") "<<endl;
	 }

	 return vec_of_pairs;
		}

std::vector<double> make_bins_gain (double gain_ll, double gain_ul, double gain_ss)

{

	std::vector<double> gain;

	for (double i = gain_ll ; i <= gain_ul ; i += gain_ss)
	{

		gain.push_back(i);
		//cout << freq[i] << ' ';


	}

	return gain;

}
std::vector<double> amp_values_read()
{
	std::vector<double> amp_vec;
   ifstream myfile;
   myfile.open("D:\\workspace\\param_file.txt");
   if (myfile.is_open())
   {
      string str = "amp";
      double a,b,c;

      while (getline(myfile,line))
	    {
	       istringstream iss(line);
	       while(iss>>str>>a>>b>>c)
			 {

	    	   if ( line.find("amp") != std::string::npos )
			     {
			    	 //amp_lower_limit=a;
			    	 //amp_upper_limit =b;
			    	 //amp_step_size = c;
			    	 amp_vec = make_bins_amps(a,b,c);
                     //std::cout<<"amp values read"<<endl;
			     }
			 }
	    }
		      myfile.close();
   }
   else cout << "Unable to open file";

   return amp_vec;
//return {amp_lower_limit,amp_upper_limit,amp_step_size};
}



std::vector<double> freq_values_read()
{
	std::vector<double> freq_vec;
   ifstream myfile;
   myfile.open("D:\\workspace\\param_file.txt");
   if (myfile.is_open())
   {
      string str = "freq";
      double a,b,c;

      while (getline(myfile,line))
	    {
	       istringstream iss(line);
	       while(iss>>str>>a>>b>>c)
			 {

	    	   if ( line.find("freq") != std::string::npos )
			     {
			    	 //amp_lower_limit=a;
			    	 //amp_upper_limit =b;
			    	 //amp_step_size = c;
			    	 freq_vec = make_bins_freq(a,b,c);
                     //std::cout<<"freq values read"<<endl;
			     }
			 }
	    }
		      myfile.close();
   }
   else cout << "Unable to open file";

   return freq_vec;
//return {amp_lower_limit,amp_upper_limit,amp_step_size};
}

std::vector<double> power_values(double r){
	std::vector<double> v = amp_values_read();
	// for_each(v.begin(), v.end(), [](double x) { return x*x; });

	//transform(v.begin(), v.end(), v.begin(), [r](double &c){ return c/r; });
     //for_each(v.begin(), v.end(), [](double y) { return 10*log10(y/1e-3); });
     for(int i=0;i<v.size();i++){
    	v[i] = 10*log10(((v[i]*v[i])/r)/1e-3);
    	 //cout<<v[i]<<"    input values bins"<<endl;
}
     return v;
  }


class stimulus_coverage : public covergroup {




public :

//	double	upper_limit;
//	double	lower_limit;
//	double	step_size;

	double Amplitude_values = 0;
	double Frequency_values = 0;
    double input_power_values_dbm =0;
    double output_power_values_dbm = 0;
    double gain_values_dbm = 0;
    double R = 50;   //resistance



CG_CONS(stimulus_coverage) {

		//	cout<< "Constructor CG_CONS called" <<endl;

		//	upper_limit = 1.0;
		//	lower_limit = 0.0;
		//	step_size = 0.05;
		//	Amplitudes = param;

}

COVERPOINT( double , Amplitude_cvp, Amplitude_values) {

	//auto result_tuple = amp_values_read();
	//bin_array< double >( "Amplitude" , make_bins_amps( amp_lower_limit, amp_upper_limit,amp_step_size))
	// bin_array< double >( "Amplitude" , make_bins_amps( 1,5, 0.5))
		bin_array< double >( "Amplitude" , amp_values_read())

	//bin_array< double >( "Amplitudes" , vec)

};


COVERPOINT( double , Frequency_cvp, Frequency_values) {

	//	bin_array< double >( "Frequency" , make_bins_freq ( 1e3,100e3,5e3))
	bin_array< double >( "Frequency" , freq_values_read())

	//bin_array< double >( "Frequency" , make_bins_freq ( freq_lower_limit, freq_upper_limit,freq_step_size))


  //bin_array< int >( "split" , 20, interval(lower_limit,upper_limit))
  //bin_array< int >( "freq_cvp" ,10, interval(0,9))
  //bin_array< double >( "Frequencies" , vec)

};
COVERPOINT( double , input_power_dbm_cvp, input_power_values_dbm) {


		bin_array< double >( "input_power" , power_values(R)) // calculate from amp_values_read()


	    //bin_array< double >( "input_power_dbm" , make_bins_power ( 13,19 , 0.01))

		//bin_array< double >( "input power", InputPower_values.size() , interval ( 0.0,InputPower_values.back()))



};

//std::vector<double> power_range = make_bins_power ( 50, 55 , 1);
COVERPOINT( double , output_power_dbm_cvp, output_power_values_dbm) {

	//bin_array< double >( "output_power_dbm" , make_bins_power ( 50, 62 , 1))

	//bin <double>("split" , power_range.size(), interval (power_range.front(),power_range.back()))

	bin_array<double>("output_power", make_intervals(50, 63 , 1))
//bin_array< double >( "16.0 dB" , 1, interval ( (power_range[0]), (power_range[1])))

		//bin_array< double >( "split" , 10, interval(50,62))
	          /*  bin <double>("low corner",interval (49.995,50.005)),
	                		bin <double>("low corner",interval (50.005,50.995)),
							bin <double>("low corner",interval (50.995,51.005)),
							bin <double>("low corner",interval (51.005,51.995)),
							bin <double>("low corner",interval (51.995,52.005)),
							bin <double>("low corner",interval (52.005,52.995)),
							bin <double>("low corner",interval (52.995,53.005)),
							bin <double>("low corner",interval (53.005,53.995)),
							bin <double>("low corner",interval (53.995,54.005)),
							bin <double>("low corner",interval (54.005,54.995)),
							bin <double>("low corner",interval (54.995,55.005)),
							bin <double>("low corner",interval (55.005,55.995)),
							bin <double>("low corner",interval (55.995,56.005)),
							bin <double>("low corner",interval (56.005,56.995)),
							bin <double>("low corner",interval (56.995,57.005)),
							bin <double>("low corner",interval (57.005,57.995)),
							bin <double>("low corner",interval (57.995,58.005)),
							bin <double>("low corner",interval (58.005,58.995)),
							bin <double>("low corner",interval (58.995,59.005)),
							bin <double>("low corner",interval (59.005,59.995)),
							bin <double>("low corner",interval (59.995,60.005)),
							bin <double>("low corner",interval (60.005,60.995)),
							bin <double>("low corner",interval (60.995,61.005)),
							bin <double>("low corner",interval (61.005,61.995)),
							bin <double>("low corner",interval (61.995,62.005)),
							bin <double>("low corner",interval (62.005,62.995))*/

																		//bin_array <double>("44.9",1, interval (40.0, 44.9)),
					//bin_array <double>("higher corner",1, interval (45.0, 49.9))
};

COVERPOINT( double , gain_cvp, gain_values_dbm) {

	//example
	//bin_array< double >( "18.0 dB", 1,interval ( 18.0, 18.4)),

	bin_array< double >( "gain" , make_intervals( 1, 15 , 1))

};

cross<double , double ,double, double > amp_freq_cross = cross< double , double, double, double > ( this ,&output_power_dbm_cvp, &input_power_dbm_cvp, &Amplitude_cvp, &Frequency_cvp
);

};


