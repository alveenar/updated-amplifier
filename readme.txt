check func_coverage.cpp

The problem is that input_power_vec and output_power_vec hold single values instead of holding powers for all combinations.
 Their size turns out to be 1 but should have 10 (amp=1,5 freq = 5e3,6e3 = (1,5000),(1,6000),(2,5000),(2,6000)....
 This results in gain very large.
 
 Second problem:
 In func_coverage file, when mux_in.read==1, the program should only print input and output power vector values in for loop but 
 it seems param_gen runs from start simulataneously,displaying amp and freq values. So when all_values_processed.write(1) in param_gen, mux_in.read()==1 gets true
 see when to bins_done.write(1) to place,  for start_processing.read(1) to be true in param_gen.
 
 Just build the project and run param_gen_test_simple_tb.exe in binaries